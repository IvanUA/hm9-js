//   1
// To creat a new element tag we need to firstly find an ellemnt using getElementById() method
// then we need to creat a new Tag by using createElement() method and after that we could
// attach new Tag to element using method appendChild().

//   2
// The first parameter of the function insertAdjacentHTML() means postion  where the HTML
//  content should be inserted in relation to the element
// "beforebegin": Inserts the HTML content immediately before the element.
// "afterbegin": Inserts the HTML content as the first child of the element.
// "beforeend": Inserts the HTML content as the last child of the element.
// "afterend": Inserts the HTML content immediately after the element.  

//   3
// We can use element.remove() to delete ellement from a page.



// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у
// вигляді списку. Завдання має бути виконане на чистому Javascript без використання 
//бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент 
// parent-DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.getElementById("myList");


function addListToDOM(array, parent = document.body){
    const list = document.createElement("ul");
    parent.appendChild(list);
    array.forEach((item) => {
        const listItem = document.createElement("li");  
        listItem.textContent = item; 
        list.appendChild(listItem); 
       });
}

addListToDOM(array, parent);


 